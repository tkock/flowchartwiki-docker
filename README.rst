===========================================
Running FlowchartWiki in a Docker Container
===========================================

For details about FlowchartWiki, an extension for MediaWiki, see http://www.flowchartwiki.org.

Copyright (c) 2015 Thomas Kock.

Licensed under the Academic Free License version 3.0. (see LICENSE-AFL-3.0.txt)

(The Academic Free License grants similar rights to the BSD, MIT and Apache licenses. See: https://en.wikipedia.org/wiki/Academic_Free_License)


Requirements
------------

- docker-compose

Remarks
-------

- uses two containers: 1: flowchartwiki, 2: MySQL, administered by docker-compose.
- To be used behind the nginx-proxy from https://github.com/jwilder/nginx-proxy
- The flowchartwiki/Dockerfile and nginx configuration are based on https://github.com/ngineered/nginx-php-fpm
  with graphviz added and git as well as some php-* packages being removed.
- tested with MediaWiki 1.25.1
- uses Docker-volumes for host-based storage of MySQL database and MediaWiki files.
- a sample LocalSettings.php is included in ``./flowchartwiki/LocalSettings-sample.php``
- nginx-site.conf is setup for the wiki to be deployed in ``/var/www/wiki``

Setup and installation
----------------------
download and unpack mediawiki into ``./flowchartwiki/volumes/var/www/wiki`` directory

- Download from https://www.mediawiki.org/wiki/Download
- Unpack the .tar.gz into ``./flowchartwiki/volumes/var/www``
- ``mv mediawiki-<version> wiki``

configure MySQL

- update the docker-compose.yml with your MySQL Settings (user,pwd)
- you might need to change or remove the mem_limit setting in docker-compose.yml, depending on your database / wiki size.
- manually initialize the MySQL database. (It might not properly initialize, when started with docker-compose the first time.)

  (replace user, password and path...)

::

  docker run --name dockerflowchartwiki_mysql_1 \
     --env='MYSQL_DATABASE=flowchartwiki' \
     --env='MYSQL_ROOT_PASSWORD=rootpass' \
     --env='MYSQL_USER=user' \
     --env='MYSQL_PASSWORD=userpass' \
     -v <path-to->/flowchartwiki-docker/mysql/volumes/var/lib/mysql:/var/lib/mysql \
     -p 3307:3306 \
     mysql:latest

build and start the container

- uncomment the port: 8000:80 statements in docker-compose.yml to temporarily enable "localhost" access.
- set the ``VIRTUAL_HOST:`` in docker-compose.yml

::

    docker-compose build
    docker-compose up

configure mediawiki

- in your browser start http://localhost:8000/wiki/
- use database settings from docker-compose.yml, db-server is "mysql"
- copy the newly created LocalSettings.php to ``flowchartwiki/volumes/var/www/wiki/``

download and setup flowchartwiki

- create directory ``flowchartwiki/volumes/var/www/wiki/extensions/flowchartwiki``
- download flowchartwiki from http://www.flowchartwiki.org/flowchartwiki-1.2.2.tar.gz
- unpack INSIDE of ``extensions/flowchartwiki``

setup mysql tables for FlowchartWiki

::

    docker exec -it flowchartwiki_mysql_1 bash
    mysql -u root -p flowchartwiki
      pass: rootpass (from docker-compose.yml)

- cut & paste the ``extensions/flowchartwiki/maintenance/schema_mysql.sql`` file contents.
- exit the container.

update LocalSettings.php

  At the end of the file add::

    # ----- flowchartwiki config -----
    # Disable cache - otherwise graphs are not updated properly
    $wgCachePages = false;
    $wgCacheEpoch = max( $wgCacheEpoch, gmdate( 'YmdHis' ) );
    # Include libraries
    require_once("$IP/extensions/flowchartwiki/flowchartwiki.php");
    $fchw['GraphvizDot'] = "/usr/bin/dot";
    # ----- flowchartwiki config -----
    # ------ nginx ----------
    $wgUsePathInfo      = true;
    # ------ END nginx ----------

create directory ``images/flowchartwiki`` in ``flowchartwiki/volumes/var/www/images``

::

  mkdir flowchartwiki
  chmod o+w flowchartwiki

Check your installation

- in MediaWiki: run special pages -> check flowchartwiki
- create test pages (see: http://www.flowchartwiki.org/wiki/index.php/Installation)

for deployment from localhost to live site

- modify LocalSettings - set wgServer.
- modify docker-compose.yml settings: port, virtual_host, memory.
- backup mysql (see below)
- copy directory structure to server (except mysql /var/lib/mysql dirs - It might work, I didn't try)
- re-create mysql and import backup
- docker-compose build
- docker-compose up

Some additional tips:

- backup of mysql "from inside" the docker-container into the filesystem / volume mounted by docker

::

  docker exec -it flowchartwiki_mysql_1 bash
  mysqldump --opt -u root -p flowchartwiki > /var/lib/mysql/dump-DATE.sql
  pass: rootpass (from docker-compose.yml)

- import of mediawiki dump

::

  docker exec -it flowchartwiki_flowchartwiki_1 bash
  cd /var/www
  php ./maintenance/importDump.php < dump.xml

